import { Component, OnInit , OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import {BookService} from '../_service/book.service';
import {Book} from '../_model/book';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit, OnDestroy {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );
  booksListSubs: Subscription;
  booksList: Book[];
  search_booksSubs: Subscription;
  searched_books: Book[];
  query;
  events = [];
  constructor(
    private breakpointObserver: BreakpointObserver,
    private booksapiservice: BookService,
    ) {}

   ngOnInit(){
    /* this.search_booksSubs = this.booksapiservice
    .search_books(this.query)
    .subscribe(response => {
        this.searched_books = response;
        console.log('response', response)
        console.log('booksList',  this.searched_books)
      },
      console.error
    ); */
    this.booksListSubs = this.booksapiservice
    .get_books()
    .subscribe(response => {
        this.booksList = response;
        console.log('response', response)
        console.log('booksList',  this.booksList)
      },
      console.error
    );
   }

    ngOnDestroy() {
      this.booksListSubs.unsubscribe();
    }
    /* search_book(){
      this.search_booksSubs = this.booksapiservice
        .search_books(this.query)
        .subscribe(response => {
            this.searched_books = response;
            console.log('response', response)
            console.log('booksList',  this.searched_books)
          },
          console.error
        );

    }
    get_books(){
      this.booksListSubs = this.booksapiservice
      .get_books()
      .subscribe(response => {
          this.booksList = response;
          console.log('response', response)
          console.log('booksList',  this.booksList)
        },
        console.error
      );
    }
 */
}
