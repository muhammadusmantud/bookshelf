import { Component ,OnInit, OnDestroy, ViewChild} from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  @ViewChild(ModalDirective) modal: ModalDirective;
  title = 'BookShelf';
 
  constructor() {
  }

  ngOnInit() {}

  ngOnDestroy() {}

}
