import { Component, OnInit , OnDestroy} from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {BookService} from '../_service/book.service';
import {Book} from '../_model/book';
import {Subscription} from 'rxjs';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit, OnDestroy {

  booksListSubs: Subscription;
  id: string;
  book: Book;
  loading = false;
  submitted = false;
  updateForm: FormGroup;
  status: any;
  errorMessage: any;
  constructor(
    private booksapiservice: BookService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    ) {
  }

  ngOnInit() {

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.get_book_by_id(this.id);
    this.updateForm = this.formBuilder.group({
      ISBN: ['', Validators.required],
      name: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required]
  });
  }
  get f() { return this.updateForm.controls; }
  ngOnDestroy() {
  }

  private get_book_by_id(id) {
    this.booksapiservice.get_book_by_id(id).pipe(first()).subscribe(book => {
    this.book = book;
    });
}

onSubmitdel() 
{
      this.submitted = true;
      this.loading = true;
      this.booksapiservice.delete_book(this.id)
      .subscribe(
       data => {
          console.log('Delete successful');
          alert('Delete successful');
   
          this.router.navigate(['/mydashboard']);

              },
           error => 
              {
               console.log(error)
               this.loading = false;
              });
            
            }

onSubmit() 
  { 
        this.submitted = true;
        console.log(this.id,'in this id in submit')
        // stop here if form is invalid
        if (this.updateForm.invalid) {
        console.log('form is invallid');
        return;
        }
        this.loading = true;
        this.book.ISBN = this.updateForm.value.ISBN;
        this.book.name = this.updateForm.value.name;
        this.book.author = this.updateForm.value.author;
        this.book.description = this.updateForm.value.description;
        this.booksapiservice.update_book(this.id, this.updateForm.value)
       .pipe(first())
       .subscribe(
        data => {
           console.log('Post successful');
    
           this.router.navigate(['/mydashboard']);
 
               },
            error => 
               {
                console.log(error)
                this.loading = false;
               });
              }
}
