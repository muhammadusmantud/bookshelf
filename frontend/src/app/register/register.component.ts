import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from '../_service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) { }


  ngOnInit() {
    this.userForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      description: ['', Validators.required]
  });
  }
  get f() { return this.userForm.controls; }
  ngOnDestroy() {
   
}

onSubmit() 
{
      this.submitted = true;

      // stop here if form is invalid
      if (this.userForm.invalid) {
      console.log('form is invallid');
      return;
      }

      this.loading = true;
      console.log(this.userForm)
      this.userService.add_user(this.userForm.value)
      .pipe(first())
      .subscribe(
       data => {
          console.log('Post successful');
   
          this.router.navigate(['/mydashboard']);

              },
           error => 
              {
               console.log(error)
               this.loading = false;
              });
            }
}



