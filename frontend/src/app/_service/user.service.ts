import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { Observable, EMPTY, throwError} from 'rxjs';
import {catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import { first } from 'rxjs/operators';
import {API_URL} from '../env';
import {User} from '../_model/user';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }
  get_users(): Observable<User[]> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
     let options = { headers: headers };
        return this.http.get<User[]>(`${API_URL}/users`,options)
      .pipe(catchError(UserService._handleError));
  }
  add_user(user: User) {
    //send post rebook to books endpoint
    let headers: HttpHeaders = new HttpHeaders();
    
    /* user.created_at = new Date().toUTCString(); 
    user.updated_at = new Date().toUTCString(); */
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('access-control-allow-origin', '*');
        let options = { headers: headers };
         
        delete user._id;
    return this.http.post(`${API_URL}/adduser`, JSON.stringify(user),options);
  }
  
  get_user_by_id(id:any) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };
    return this.http.get<User>(`${API_URL}/getid/`+ id ,options);
  }
 
  update_user(id: any, user: User){
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };

    /*   */
    return this.http.put<User>(`${API_URL}/updateuser/`+ id,JSON.stringify(user) ,options);
  }
  delete_user(id){
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };
    return this.http.delete<User>(`${API_URL}/deleteuser/`+ id ,options);
  }
}