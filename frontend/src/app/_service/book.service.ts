import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { Observable, EMPTY, throwError} from 'rxjs';
import {catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import { first } from 'rxjs/operators';
import {API_URL} from '../env';
import {Book} from '../_model/book';

@Injectable()
export class BookService {

  constructor(private http: HttpClient) {
  }

  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }

  // GET list of public, future events
  get_books(): Observable<Book[]> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
     let options = { headers: headers };
        return this.http.get<Book[]>(`${API_URL}/books`,options)
      .pipe(catchError(BookService._handleError));
  }

  add_book(book: Book) {
    //send post rebook to books endpoint
    let headers: HttpHeaders = new HttpHeaders();
    //book.statusUpdatedAt = new Date().toUTCString();
    
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('access-control-allow-origin', '*');
        let options = { headers: headers };
        delete book._id;
    return this.http.post(`${API_URL}/books`, JSON.stringify(book),options);
  }
  get_book_by_id(id:any) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };
    return this.http.get<Book>(`${API_URL}/getid/`+ id ,options);
  }
 
  update_book(id: any, book: Book){
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };

    /*   */
    return this.http.put<Book>(`${API_URL}/updatebook/`+ id,JSON.stringify(book) ,options);
  }
  delete_book(id){
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };
    return this.http.delete<Book>(`${API_URL}/deletebook/`+ id ,options);
  }

  search_books(query): Observable<Book[]> {
    //get all books from books endpoint      
        
          let headers: HttpHeaders = new HttpHeaders();
          headers = headers.append('Content-Type', 'application/json');
          headers = headers.append('Access-Control-Allow-Origin', '*');
          let options = { headers: headers };
          return this.http.get<Book[]>(`${API_URL}/search/`+query,options);
        }
}