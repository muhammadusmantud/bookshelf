import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBookComponent } from '././add-book/add-book.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BookComponent } from './book/book.component';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';
import { SearchbooksComponent } from './searchbooks/searchbooks.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { EditprofileComponent } from './editprofile/editprofile.component';

const routes: Routes = [
  { path: 'addbook', component: AddBookComponent},
  { path: 'navigation', component: NavigationComponent},
  { path: 'book/:id', component: BookComponent},
  { path: 'mydashboard', component: MyDashboardComponent},
  { path: 'search/:query', component: SearchbooksComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'edituser/:id', component: EditprofileComponent},
  {path: '**', redirectTo: '/mydashboard'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
