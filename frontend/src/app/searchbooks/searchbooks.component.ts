import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import {BookService} from '../_service/book.service';
import {Book} from '../_model/book';
import {Subscription} from 'rxjs';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';


@Component({
  selector: 'app-searchbooks',
  templateUrl: './searchbooks.component.html',
  styleUrls: ['./searchbooks.component.css']
})
export class SearchbooksComponent implements OnInit, OnDestroy {
  search_booksSubs: Subscription;
  searched_books: Book[];
  query;

  constructor(
    private booksapiservice: BookService,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    
    this.activatedRoute.paramMap.subscribe((params: ParamMap) =>{ 
     this.query = this.activatedRoute.snapshot.paramMap.get('query');
     this.search_books(this.query)  

     console.log('parameteris : '+this.query); //business logic what changes you want on the page with this new data. });
  });
    
  }
  ngOnDestroy() {
    this.search_booksSubs.unsubscribe();
  }


  search_books(query){
    console.log(query)
  this.search_booksSubs = this.booksapiservice
    .search_books(query)
    .subscribe((response: any) => {
        this.searched_books = response;
        console.log('response in search book', response)
        console.log('booksList in search book',  this.searched_books)
        this.cdr.detectChanges();
      },
      console.error
    );
  }

}
