import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { BookService } from '../_service/book.service';


@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  bookForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private bookService: BookService
  ) { }
  ngOnInit() {
    this.bookForm = this.formBuilder.group({
      ISBN: ['', Validators.required],
      name: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required]
  });
  }
  get f() { return this.bookForm.controls; }
  ngOnDestroy() {
   
}

onSubmit() 
{
      this.submitted = true;

      // stop here if form is invalid
      if (this.bookForm.invalid) {
      console.log('form is invallid');
      return;
      }

      this.loading = true;
      console.log(this.bookForm)
      this.bookService.add_book(this.bookForm.value)
      .pipe(first())
      .subscribe(
       data => {
          console.log('Post successful');
   
          this.router.navigate(['/mydashboard']);

              },
           error => 
              {
               console.log(error)
               this.loading = false;
              });
            }
}



