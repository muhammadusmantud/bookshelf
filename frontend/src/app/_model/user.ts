export class User {     
    public firstName: number;
    public lastName: string;
    public email: string;
    public password: string;
    public description: string;
    public _id?: number;
  }