export class Book {     
      public ISBN: number;
      public name: string;
      public author: string;
      public description: string;
      public _id?: number;
    }