import { RouterModule } from '@angular/router'; // we also need angular router for Nebular to function properly
import { NgModule } from '@angular/core';



@NgModule({
  imports: [
    RouterModule, 
  ],
  providers: [],
})
export class NavigationModule {}
