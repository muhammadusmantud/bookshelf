from sqlalchemy import Column, String

from .entity import Entity, Base
from marshmallow import Schema, fields


class Book(Entity, Base):
    __tablename__ = 'books'

    ISBN = Column(String)
    name = Column(String)
    author = Column(String)
    description = Column(String)

    def __init__(self, ISBN,name,author, description):
        Entity.__init__(self)
        self.ISBN = ISBN
        self.name = name
        self.author = author
        self.description = description

class BookSchema(Schema):
    id = fields.Int()
    ISBN = fields.Str()
    name = fields.Str()
    author = fields.Str()
    description = fields.Str()