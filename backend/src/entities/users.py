from sqlalchemy import Column, String

from .entity import Entity, Base
from marshmallow import Schema, fields


class User(Entity, Base):
    __tablename__ = 'users'

    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    password = Column(String)
    description = Column(String)

    def __init__(self, first_name,last_name,email,password, description):
        Entity.__init__(self)
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        self.description = description

class UserSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Str()
    password = fields.Str()
    description = fields.Str()