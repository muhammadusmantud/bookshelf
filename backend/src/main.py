from entities.entity import Session, engine, Base
from entities.books import Book, BookSchema
from entities.users import User, UserSchema
from flask import Flask, jsonify, request
from flask_cors import CORS
# import module sys to get the type of exception
import sys

# creating the Flask application
app = Flask(__name__)
CORS(app)

# generate database schema
Base.metadata.create_all(engine)

# start session
session = Session()

# check for existing data
books = session.query(Book).all()

if len(books) == 0:
    # create and persist mock book
    python_book = Book("1151513","Data Structures","Black Widow" ,"Test your knowledge about Data and murders.")
    session.add(python_book)
    session.commit()
    session.close()

@app.route('/adduser', methods=['POST'])
def add_user():
    try:
        new_user = UserSchema(only=('first_name','last_name','email','password','description')).load(request.get_json())
        new_user['first_name'] = new_user['first_name'] 
        new_user['last_name'] = new_user['last_name']
        new_user['email'] = new_user['email']
        new_user['password'] = new_user['password']
        new_user['description'] = new_user['description']
        user = User(**new_user)
        print('user',user)
        
        # persist user
        session = Session()
        session.add(user)
        print('between session add and session commit')
        session.commit()

        # return created user
        reg_user = UserSchema().dump(user)
        print('reguser',reg_user)
        session.close()
        return jsonify(reg_user), 201

    except Exception as e:
        #print("Oops!", e.__class__, "occurred.")
        print("Oops!", e, "error here.")
        return e 


@app.route('/books')
def get_books():
    try:
        # fetching from the database
        session = Session()
        book_objects = session.query(Book).all()

        # transforming into JSON-serializable objects
        schema = BookSchema(many=True)
        books = schema.dump(book_objects)

        # serializing as JSON
        session.close()
        return jsonify(books)
    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e



@app.route('/getid/<int:_id>')
def get_book_by_id(_id):
    try:
        # fetching from the database
        session = Session()
        book_object = session.query(Book).get(_id)
        
        # transforming into JSON-serializable objects
        schema = BookSchema()
        selected_book = schema.dump(book_object)
        print(selected_book,type(selected_book))

        # serializing as JSON
        session.close()
        print(selected_book,type(selected_book))
        return jsonify(selected_book)
    
    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e 
        

@app.route('/updatebook/<int:_id>', methods=['PUT'])
def update_book(_id):
    try:
            
        posted_book = BookSchema(only=('ISBN','name','author','description')).load(request.get_json())
        updated_book = Book(**posted_book)
        
        # fetching from the database
        session = Session()
        book_object = session.query(Book).get(_id)

        #update the values from form data
        book_object.ISBN = updated_book.ISBN
        book_object.name = updated_book.name
        book_object.author = updated_book.author
        book_object.description = updated_book.description
        session.commit()
        # return created book
        returned_book = BookSchema().dump(book_object)
        session.close()
        print('bok object in update', returned_book)
        return jsonify(returned_book),200
    
    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e    

@app.route('/deletebook/<int:_id>', methods=['DELETE'])
def delete_book(_id):
    try:
        # fetching from the database
        session = Session()
        book_object = session.query(Book).get(_id)
        session.delete(book_object)
        session.commit()
        # return created book
        returned_book = BookSchema().dump(book_object)
        session.close()
        print('bok object in delete', returned_book)
        return jsonify(returned_book),200
    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e    
@app.route('/books', methods=['POST'])
def add_book():
    try:
        # mount book object
        posted_book = BookSchema(only=('ISBN','name','author','description')).load(request.get_json())
        posted_book['name'] = posted_book['name'].casefold()  
        posted_book['author'] = posted_book['author'].casefold()  
        posted_book['description'] = posted_book['description'].casefold()  
        book = Book(**posted_book)
        
        # persist book
        session = Session()
        session.add(book)
        session.commit()

        # return created book
        new_book = BookSchema().dump(book)
        session.close()
        return jsonify(new_book), 201

    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e    

@app.route('/search/<search_query>', methods=['GET'])
def search_book(search_query):
    try:
        #search case insensitive + with spaces
        session = Session()
        
        #Get argument from query form
        searched_book_query = session.query(Book).filter(
            ( Book.ISBN.like(search_query)|Book.name.like(search_query)|Book.author.like(search_query))).all()
        
        # transforming into JSON-serializable objects
        schema = BookSchema(many=True)
        searched_book = schema.dump(searched_book_query)
        session.close()
        return jsonify(searched_book)
    except Exception as e:
        print("Oops!", e.__class__, "occurred1.")
        return e    

""" @app.errorhandler(404)
def not_found_error(error):
    return  '404'

@app.errorhandler(500)
def internal_error(error):
    return  '500' """

if __name__ == "__main__":
    #app.run(host="0.0.0.0",port='5000',threaded=true, debug=True)^
    app.run(port=5000, threaded=True,host='0.0.0.0')