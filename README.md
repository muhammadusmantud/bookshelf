# BookShelf
* The BookShelf application performs as following:
* It stores a new book item in the system. Each book has an International Standard Book Number (ISBN), name, authors and a short annotation.
* It can update the book’s properties.
* It can delete a book.
* It can search for books by ISBN, name or author.

# BookShelf Dashboard consists of a Javascript Frontend built with Angular9 and a Python backend server which communicates between themselves with Flask API. 

## Stack Versions
* Python: 3.9
* Angular CLI: 11.0.6
* Node: 14.15.4
* Flask: 1.1.2
* OS: win32 x64
* Docker
* POSTGRES

## Development Setup
* Git clone repository.
* from cloned diretory run 'docker-compose up -d --build'
* Switch to your browser and goto address: http://localhost:8080


## Future Work

* Fix yaml file for CI/CD
* Make the rows in table clickable (instead of clicking on Details button)
* add separate components for 404 error pages
